// This file is part of the dabet source code.
//
// ©️ 2022 Anna Aurora <mailto:anna@annaaurora.eu> <https://matrix.to/#/@papojari:artemislena.eu> <https://annaaurora.eu>
//
// For the license information, please view the README.md file that was distributed with this source code.

use clap::Parser;
use recolored::*;
use chrono::{DateTime};
use chrono::format::ParseError;

#[derive(Parser, Debug)]
#[clap(name = "dabet", version, about="Print the duration between two times")]
struct Args {
    /// Set the start time in a ISO8601 format time string.
    #[clap(long, short)]
    start_time: String,

    /// Set the end time in a ISO8601 format time string.
    #[clap(long, short)]
    end_time: String,

    /// Don't print the unit indicator.
    #[clap(long, short)]
    no_unit: bool,
}

fn main() -> Result<(), ParseError> {
    let args = Args::parse();
    let start_time = DateTime::parse_from_rfc3339(args.start_time.as_str())?;
    let end_time = DateTime::parse_from_rfc3339(args.end_time.as_str())?;
    let no_unit = args.no_unit;

    let duration = end_time - start_time;

    if no_unit {
        println!("{}", duration.num_seconds());
    } else {
        println!("{} s", duration.num_seconds());
    };

    Ok(())
}
